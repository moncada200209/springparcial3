package com.parcialBsme.instituto.entitys;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
@Entity
public class Recibo {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String urlArchivo;
    
    @OneToOne
    @JoinColumn(name = "idea_proyecto_id")
    private IdeaProyecto ideaProyecto;
    
    @ManyToOne
    @JoinColumn(name = "estudiante_id")
    private Usuario estudiante;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrlArchivo() {
        return urlArchivo;
    }

    public void setUrlArchivo(String urlArchivo) {
        this.urlArchivo = urlArchivo;
    }

    public IdeaProyecto getIdeaProyecto() {
        return ideaProyecto;
    }

    public void setIdeaProyecto(IdeaProyecto ideaProyecto) {
        this.ideaProyecto = ideaProyecto;
    }

    public Usuario getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Usuario estudiante) {
        this.estudiante = estudiante;
    }

    
}
